# FreeCAD User Conf 2023

This repo contains the example files used in my Advanced Parametric Modelling presentation.

The presentation video can be found here; https://youtu.be/Yp6cIMA7LsI


## Planetary Gearset

![Planetary Gearset](Images/Screenshot from 2023-03-02 22-55-08.png)

A planetary gear set used to demonstrate a robust, user customisable planetary gearset. It is not intended to be a functional gearset, just an assembly example.
This example consists of an assembly (PlanetaryGearAsm.FCStd) and the sub folder "Parts" containing individual files for each gear type and a parameter spreadsheet.

If the main assembly sketch fails to resolve, you may need to enable "Show Section 'Advanced Solver Controls'" in the sketcher tab of the preferences. Then enable "Sketch Size Multiplier" in the advanced controls for that sketch.
(Screenshots of this coming soon)

**Requirements:** This example requires the Assembly 4 workbench, Fasteners workbench and Gear workbench


## Parametric Enclosure

![Parametric Enclosure](Images/Screenshot from 2023-03-02 23-01-36.png)

A functional, customisable enclosure with mounting holes.

**Requirements:** This example requires the Fasteners workbench


## Binder Based Extrusion

![2020 Extrusion](Images/Screenshot from 2023-03-02 23-12-34.png)

An example of how a Pad (or other operations) can be changed dynamically to a multitude of different shapes using expressions and a library of sketches and shape binders.

A drop down is used to select which shape is used to create the extrusion, the drop down is attached to the origin of the VSlot body (to avoid cyclic dependency issues in a simple manner) as shown in the following image.
![Location of dropdown to change extrusion types](Images/Screenshot from 2023-03-02 22-56-47.png)


## Parametric Threads

![Parametric Hole Example](Images/Screenshot from 2023-03-02 23-00-55.png)

An example of how to change the size of a Part Design Hole / Threaded hole using expressions
